﻿using System;
using System . Collections . Generic;
using System . Net;
using System . Text . RegularExpressions;

namespace TVPDL {
	class Program {
		static void Main ( string [ ] args ) {
			if ( args . Length == 0 ) {
				Console . WriteLine ( 
					"Usage:" +
					"\r\n TVPDL [URL]"
					);
			} else {
				// Base URL
				// https://vod.tvp.pl/video/NAME[,EP],ID

				// URL with player link
				// https://vod.tvp.pl/sess/tvplayer.php?object_id=ID&autoplay=true&nextprev=1

				if ( !args [ 0 ] . Contains ( "vod.tvp.pl/video/" ) ) {

					Console . WriteLine ( "Invalid URL. URL needs to contain \"vod.tvp.pl\" and link to a video." );
					Environment . Exit ( 404 );
				}
				string[] a = null;

				if ( args [ 0 ] . Contains ( "," ) )
				{
					a = args [ 0 ] . Split ( ',' );
				} else {
					Console . WriteLine ( "Invalid URL. URL needs to link to a video.\r\nExample of valid URL: https://vod.tvp.pl/video/example,odc-1,999999999" );
					Environment . Exit ( 100 );
				}
				var b = a[a.Length-1];
				Console . WriteLine ( "Video ID: " + b );
				var c = "https://vod.tvp.pl/sess/tvplayer.php?object_id="+b+"&autoplay=true&nextprev=1";
				Console . WriteLine ( "Player: " + c);
				string contents;
				using ( var wc = new System . Net . WebClient ( ) )
					contents = wc . DownloadString ( c );
				var x = new List<string>();
				var linkParser = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
				foreach ( Match m in linkParser . Matches ( contents ) )
					x . Add ( m . Value );
				var e = "";
				foreach ( var d in x ) {
					if ( d . Contains ( ".mp4" ) ) {
						Console . WriteLine ( "Video: " + d );
						e = d;
					}
				}
				Console . WriteLine ( "Downloading..." );
				using ( var client = new WebClient ( ) )
					client . DownloadFile ( e , "download-"+b+".mp4" );
			}

		}
	}
}
